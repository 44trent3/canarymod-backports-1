package net.minecraft.network.rcon;


import net.canarymod.config.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;


public class RConThreadClient extends RConThreadBase {

    private static final Logger h = LogManager.getLogger();
    private boolean i;
    private Socket j;
    private byte[] k = new byte[1460];
    private String l;

    RConThreadClient(IServer iserver, Socket socket) {
        super(iserver, "RCON Client");
        this.j = socket;

        try {
            this.j.setSoTimeout(0);
        } catch (Exception exception) {
            this.a = false;
        }

        this.l = Configuration.getServerConfig().getRconPassword();
        this.b("Rcon connection from: " + socket.getInetAddress());
    }

    public void run() {
        try {
            while (this.a) {

                BufferedInputStream bufferedinputstream = new BufferedInputStream(this.j.getInputStream());
                int i0 = bufferedinputstream.read(this.k, 0, 1460);

                if (10 <= i0) {
                    byte b0 = 0;
                    int i1 = RConUtils.b(this.k, 0, i0);

                    if (i1 != i0 - 4) {
                        return;
                    }

                    int i2 = b0 + 4;
                    int i3 = RConUtils.b(this.k, i2, i0);

                    i2 += 4;
                    int i4 = RConUtils.b(this.k, i2);

                    i2 += 4;
                    switch (i4) {
                        case 2:
                            if (this.i) {
                                String s0 = RConUtils.a(this.k, i2, i0);

                                try {
                                    this.a(i3, this.b.g(s0));
                                } catch (Exception exception) {
                                    this.a(i3, "Error executing: " + s0 + " (" + exception.getMessage() + ")");
                                }
                                continue;
                            }

                            this.f();
                            continue;

                        case 3:
                            String s1 = RConUtils.a(this.k, i2, i0);
                            int i5 = i2 + s1.length();

                            if (0 != s1.length() && s1.equals(this.l)) {
                                this.i = true;
                                this.a(i3, 2, "");
                                continue;
                            }

                            this.i = false;
                            this.f();
                            continue;

                        default:
                            this.a(i3, String.format("Unknown request %s", new Object[] { Integer.toHexString(i4)}));
                            continue;
                    }
                }
            }

        } catch (SocketTimeoutException sockettimeoutexception) {
        } catch (IOException ioexception) {
        } catch (Exception exception1) {
            h.error("Exception whilst parsing RCON input", exception1);
        } finally {
            this.g();
        }

    }

    private void a(int i0, int i1, String s0) throws IOException {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(1248);
        DataOutputStream dataoutputstream = new DataOutputStream(bytearrayoutputstream);
        byte[] abyte = s0.getBytes("UTF-8");

        dataoutputstream.writeInt(Integer.reverseBytes(abyte.length + 10));
        dataoutputstream.writeInt(Integer.reverseBytes(i0));
        dataoutputstream.writeInt(Integer.reverseBytes(i1));
        dataoutputstream.write(abyte);
        dataoutputstream.write(0);
        dataoutputstream.write(0);
        this.j.getOutputStream().write(bytearrayoutputstream.toByteArray());
    }

    private void f() throws IOException {
        this.a(-1, 2, "");
    }

    private void a(int i0, String s0) throws IOException {
        int i1 = s0.length();

        do {
            int i2 = 4096 <= i1 ? 4096 : i1;

            this.a(i0, 0, s0.substring(0, i2));
            s0 = s0.substring(i2);
            i1 = s0.length();
        } while (0 != i1);

    }

    private void g() {
        if (null != this.j) {
            try {
                this.j.close();
            } catch (IOException ioexception) {
                this.c("IO: " + ioexception.getMessage());
            }

            this.j = null;
        }
    }

}
