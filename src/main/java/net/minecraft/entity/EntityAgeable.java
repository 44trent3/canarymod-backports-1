package net.minecraft.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public abstract class EntityAgeable extends EntityCreature {
    private float bp = -1.0F;
    private float bq;

    // CanaryMod start
    @Override
    public void inactiveTick() {
        super.inactiveTick();
        if (this.o.E) {
            this.a(this.f);
        } else {
            int i = this.d();

            if (i < 0) {
                ++i;
                this.c(i);
            } else if (i > 0) {
                --i;
                this.c(i);
            }
        }
    }
    // CanaryMod end -- THIS METHOD IS THE ONLY DIFFERENCE. Everything else is decompiled!

    public EntityAgeable(World world) {
        super(world);
    }

    public abstract EntityAgeable a(EntityAgeable var1);

    public boolean a(EntityPlayer entityplayer) {
        ItemStack itemstack = entityplayer.bm.h();
        if (itemstack != null && itemstack.b() == Items.bx) {
            if (!this.o.E) {
                Class oclass0 = EntityList.a(itemstack.k());
                if (oclass0 != null && oclass0.isAssignableFrom(this.getClass())) {
                    EntityAgeable entityageable = this.a(this);
                    if (entityageable != null) {
                        entityageable.c(-24000);
                        entityageable.b(this.s, this.t, this.u, 0.0F, 0.0F);
                        this.o.d(entityageable);
                        if (itemstack.u()) {
                            entityageable.a((String)itemstack.s());
                        }

                        if (!entityplayer.bE.d) {
                            --itemstack.b;
                            if (itemstack.b <= 0) {
                                entityplayer.bm.a(entityplayer.bm.c, (ItemStack)null);
                            }
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    protected void c() {
        super.c();
        this.af.a(12, new Integer(0));
    }

    public int d() {
        return this.af.c(12);
    }

    public void a(int i0) {
        int i1 = this.d();
        i1 += i0 * 20;
        if (i1 > 0) {
            i1 = 0;
        }

        this.c(i1);
    }

    public void c(int i0) {
        this.af.b(12, i0);
        this.a(this.f());
    }

    public void b(NBTTagCompound nbttagcompound) {
        super.b(nbttagcompound);
        nbttagcompound.a("Age", this.d());
    }

    public void a(NBTTagCompound nbttagcompound) {
        super.a(nbttagcompound);
        this.c(nbttagcompound.f("Age"));
    }

    public void e() {
        super.e();
        if (this.o.E) {
            this.a(this.f());
        } else {
            int i0 = this.d();
            if (i0 < 0) {
                ++i0;
                this.c(i0);
            } else if (i0 > 0) {
                --i0;
                this.c(i0);
            }
        }

    }

    public boolean f() {
        return this.d() < 0;
    }

    public void a(boolean flag0) {
        this.a(flag0 ? 0.5F : 1.0F);
    }

    protected final void a(float f0, float f1) {
        boolean flag0 = this.bp > 0.0F;
        this.bp = f0;
        this.bq = f1;
        if (!flag0) {
            this.a(1.0F);
        }

    }

    protected final void a(float f0) {
        super.a(this.bp * f0, this.bq * f0);
    }
}