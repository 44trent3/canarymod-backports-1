package net.minecraft.nbt;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTSizeTracker;


public class NBTTagEnd extends NBTBase {

    void a(DataInput datainput, int i0, NBTSizeTracker nbtsizetracker) throws IOException {
        nbtsizetracker.a(64L);
    }

    void a(DataOutput dataoutput) throws IOException {}

    public byte a() {
        return (byte) 0;
    }

    public String toString() {
        return "END";
    }

    public NBTBase b() {
        return new NBTTagEnd();
    }
}
